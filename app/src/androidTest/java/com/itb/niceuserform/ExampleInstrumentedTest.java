package com.itb.niceuserform;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static com.itb.niceuserform.MaterialTestMatchers.hasTextInputLayoutErrorText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void correctLogin(){
        onView(withId(R.id.btnWelcomeLogin))
                .perform(click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.etUsername))
                .perform(replaceText("user1"));
        onView(withId(R.id.etPassword))
                .perform(replaceText("user1"));

        onView(withId(R.id.btnLLogin))
                .perform(click());
        onView(withId(R.id.userLoggedFragment))
                .check(matches(isDisplayed()));
    }

    @Test
    public void incorrectLogin(){
        onView(withId(R.id.btnWelcomeLogin))
                .perform(click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.etUsername))
                .perform(replaceText("user2"));
        onView(withId(R.id.etPassword))
                .perform(replaceText("user2"));

        onView(withId(R.id.btnLLogin))
                .perform(click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));
    }

    @Test
    public void nullLogin(){
        onView(withId(R.id.btnWelcomeLogin))
                .perform(click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnLLogin))
                .perform(click());

        onView(withId(R.id.tilLUsername))
                .check(matches(hasTextInputLayoutErrorText(R.string.required_field)));
        onView(withId(R.id.tilLPassword))
                .check(matches(hasTextInputLayoutErrorText(R.string.required_field)));
    }

    @Test
    public void correctRegister(){
        onView(withId(R.id.btnWelcomeRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.etRUsername))
                .perform(replaceText("user2"));
        onView(withId(R.id.etRPassword))
                .perform(replaceText("pass"));
        onView(withId(R.id.etRPassword2))
                .perform(replaceText("pass"));
        onView(withId(R.id.etREmail))
                .perform(replaceText("user@email.com"));

        onView(withId(R.id.btnRRegister))
                .perform(scrollTo(), click());
        onView(withId(R.id.userLoggedFragment))
                .check(matches(isDisplayed()));
    }

    @Test
    public void incorrectRegister(){
        onView(withId(R.id.btnWelcomeRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.etRUsername))
                .perform(replaceText(""));
        onView(withId(R.id.etRPassword))
                .perform(replaceText("password"));
        onView(withId(R.id.etRPassword2))
                .perform(replaceText("pass"));
        onView(withId(R.id.etREmail))
                .perform(replaceText("useremailcom"));

        onView(withId(R.id.btnRRegister))
                .perform(scrollTo(), click());

        onView(withId(R.id.tilRUsername))
                .check(matches(hasTextInputLayoutErrorText(R.string.required_field)));
        onView(withId(R.id.tilRPassword))
                .check(matches(hasTextInputLayoutErrorText(R.string.pass_not_match)));
        onView(withId(R.id.tilRPAssword2))
                .check(matches(hasTextInputLayoutErrorText(R.string.pass_not_match)));
        onView(withId(R.id.tilREmail))
                .check(matches(hasTextInputLayoutErrorText(R.string.wrong_email_format)));
    }

    @Test
    public void user1Register(){
        onView(withId(R.id.btnWelcomeRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.etRUsername))
                .perform(replaceText("user1"));
        onView(withId(R.id.etRPassword))
                .perform(replaceText("pass"));
        onView(withId(R.id.etRPassword2))
                .perform(replaceText("pass"));
        onView(withId(R.id.etREmail))
                .perform(replaceText("user1@email.com"));

        onView(withId(R.id.btnRRegister))
                .perform(scrollTo(), click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

    }

    @Test
    public void navigationLoginToRegister(){
        onView(withId(R.id.btnWelcomeLogin))
                .perform(click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnLRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnRLogin))
                .perform(scrollTo(), click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));
    }

    @Test
    public void navigationRegisterToLogin(){
        onView(withId(R.id.btnWelcomeRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnRLogin))
                .perform(scrollTo(), click());
        onView(withId(R.id.loginFragment))
                .check(matches(isDisplayed()));

        onView(withId(R.id.btnLRegister))
                .perform(click());
        onView(withId(R.id.registerFragment))
                .check(matches(isDisplayed()));
    }

}
